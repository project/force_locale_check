<?php

namespace Drush\Commands\force_locale_check;

use Consolidation\AnnotatedCommand\AnnotationData;
use Consolidation\AnnotatedCommand\CommandData;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;

/**
 * Backend drush commands.
 */
class ForceLocaleCheckCommand extends DrushCommands
{
  /**
   * Add option to locale:check command.
   *
   * @hook option locale:check
   *
   * @param \Symfony\Component\Console\Command\Command     $command
   * @param \Consolidation\AnnotatedCommand\AnnotationData $annotationData
   */
    public function forceLocaleCheckOption(Command $command, AnnotationData $annotationData): void
    {
        $command->addOption(
            'force',
            '',
            InputOption::VALUE_NONE,
            'Force download of translation files.'
        );
    }


  /**
   * Reset language information before checking for locale updates.
   * @hook pre-command locale:check
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   */
    public function forceLocaleCheckCommand(CommandData $commandData): void
    {
        if ($commandData->input()->getOption('force')) {
            locale_translation_clear_status();
            locale_translation_file_history_delete();
        }
    }
}
